package jrtt.imust.com.jrtt.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.adapter.PicQuickAdapter;
import jrtt.imust.com.jrtt.bean.PicData;
import jrtt.imust.com.jrtt.net.JrttApi;
import jrtt.imust.com.jrtt.net.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
组图页面
     时就以一个列表的方式显示图片的集合
     使用ListView
     1.到布局fragment_pic里面添加一个<ListView
     2.回到PicFragment查找View
     3.获取服务端的数据
     4.将json转换成集合
     5.创建一个adapter
     6.设置给ListView
 */
public class PicFragment未实现接口 extends BaseFragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //1.到布局fragment_pic里面添加一个<ListView
        View view = View.inflate(getContext(), R.layout.fragment_pic,null);
        //2.回到PicFragment查找View
        final ListView listView = view.findViewById(R.id.listview);
        //3.获取服务端数据
        //http://localhost:8080/jrtt/photos/photos_1.json
        String baseUrl = "http://10.0.2.2:8080/jrtt/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();

        //读取接口中的配置的参数
        JrttApi api = retrofit.create(JrttApi.class);
        Call<ResponseData> call = api.getDataPic();
        //System.out.println(api.getDataPic());
        //call可以放在线程队列里面等一个线程
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                ResponseData responseData = response.body();
                System.out.println("获取数据成功:"+responseData.data);
                // 4，将json转成集合
                //解析json数据使用gson库
                Gson gson = new Gson();
                PicData picData = gson.fromJson(responseData.data,PicData.class);
                //5.创建一个adapter
                PicQuickAdapter adapter =new PicQuickAdapter(getContext(),R.layout.item_pic,picData.news);
                //6.设置给ListView
                listView.setAdapter(adapter);
                System.out.println(picData.news.size());
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                //如果请求失败，提示
                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
