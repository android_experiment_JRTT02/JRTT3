package jrtt.imust.com.jrtt.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;

import java.util.List;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.bean.PicData;

/**
 * Created by 10566 on 2020/6/18.
 */

public class PicQuickAdapter extends QuickAdapter<PicData.NewsBean>{
    public PicQuickAdapter(Context context, int layoutResId, List<PicData.NewsBean> data) {
        super(context, layoutResId, data);
    }

    @Override
    protected void convert(BaseAdapterHelper helper, PicData.NewsBean item) {
        //显示文字
        helper.setText(R.id.text,item.title);
        ImageView imageView = helper.getView(R.id.image);
        //图片
        Glide.with(context).load(item.listimage).into(imageView);
    }

}
