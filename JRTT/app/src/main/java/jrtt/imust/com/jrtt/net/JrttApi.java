package jrtt.imust.com.jrtt.net;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by 10566 on 2020/6/18.
 */

public interface JrttApi {
    //@GET表示使用get请求
    //括号内写的是后半段的路径
    //http://10.0.2.2:8080/jrtt/photos/photos_1.json
    @GET("photos/photos_1.json")
    Call<ResponseData> getDataPic();

    //http://10.0.2.2:8080/jrtt/video.json
    @GET("video.json")
    Call<ResponseData> getVideoData();

    //http://10.0.2.2:8080/jrtt/home.json
    @GET("home.json")
    Call<ResponseData> getDataNews();

    //http://10.0.2.2:8080/10007//list_1.json
    /*@GET("10007/list_1.json")
    Call<ResponseData> getDataOneChannelNews();*/
    @GET("{id}/{page}")//@GET里面{}表示占位，将来有真实的数据来替换
    Call<ResponseData> getDataOneChannelNews(@Path("id") String id,@Path("page") String page);//数据传过来
    //必须  指定替换 10007  {id}   list_1.json 替换{page}   10007/list_1.json
}
