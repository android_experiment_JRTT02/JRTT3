package jrtt.imust.com.jrtt.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.fragment.UserFragment;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
         //编写页面内容
        //如果我开发一个PicFragment，我就把组图页面放在这个activity显示
        Fragment picFragment = new UserFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frame,picFragment).commit();
        //参数一，指定布局  参数二，Fragment

        /*//这里测试
        JrttApi api = RetrofitUtil.get();
        Call<ResponseData> call = api.getVideoData();
        RetrofitUtil.send(this,call);*/
    }
}
