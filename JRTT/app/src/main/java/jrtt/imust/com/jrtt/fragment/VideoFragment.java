package jrtt.imust.com.jrtt.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.adapter.VideoQuickAdapter;
import jrtt.imust.com.jrtt.bean.VideoData;
import jrtt.imust.com.jrtt.net.JrttApi;
import jrtt.imust.com.jrtt.net.ResponseData;
import jrtt.imust.com.jrtt.net.RetrofitUtil;
import jrtt.imust.com.jrtt.net.onDataLodListener;
import retrofit2.Call;

/*
当前我们在这里开发一个视频列表的页面
    1.在fragment_video布局文件中，fangyige<ListView...
    2.可以访问video.json
    3.协商一个Retrofit的请求代码
    4.解析这个json数据
    5.不能直接把集合设置给ListView,而要创建一个ADapter
    6.再设置给列表ListView
 */
public class VideoFragment extends BaseFragment implements onDataLodListener{

    private View v;
    private ListView listView;
    private VideoData video;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        initViews();
        requestNet();
        return v;

    }

    @Override
    public void initViews() {
        //1.在fragment_video布局文件中，fangyige<ListView...
        v = View.inflate(getContext(), R.layout.fragment_video,null);
        //2.可以访问video.json
        listView = v.findViewById(R.id.listview);
    }

    @Override
    public void requestNet() {
        JrttApi api = RetrofitUtil.get();
        Call<ResponseData> call = api.getVideoData();
        RetrofitUtil.send(getContext(),call,VideoFragment.this);
    }

    @Override
    public void parsejson(String json) {
        //4.解析这个json数据
        Gson gson = new Gson();
        video = gson.fromJson(json, VideoData.class);
        System.out.print("有多少集："+ video.data.vlist.size());//75
    }

    @Override
    public void showData() {
        //5.不能直接把集合设置给ListView,而要创建一个ADapter
        VideoQuickAdapter adapter =new VideoQuickAdapter(getContext(),R.layout.item_video,video.data.vlist);
        //6.再设置给列表ListView
        listView.setAdapter(adapter);
    }
}
