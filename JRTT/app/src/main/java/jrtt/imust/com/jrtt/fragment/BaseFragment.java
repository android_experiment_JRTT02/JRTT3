package jrtt.imust.com.jrtt.fragment;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import jrtt.imust.com.jrtt.R;

/**
 Fragment是一个可以被包含在 FragmentActivity里面的视图
 1，一般我们使用support-v4包里面
 2, 如果你创建一个Activity,配置layout文件，编写逻辑
 得到功能清单里面AndroidManifest.xml里面配置
 一般情况是ok
 如果一个Activity比较复杂，建议大家使用Frgament
 小Activity
 创建一个Fragment,配置layout文件，编写逻辑
 不需要到AndroidManifest.xml里面配置，
 只需要通过FragmentManger,可以添加一个布局
 */

public class BaseFragment extends Fragment {


    @TargetApi(Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //将layout转换成view
        View view = View.inflate(getContext(), R.layout.fragment_newschannel,null);
        //参数一，上下文  参数二，配置文件  参数三，null
        TextView textView = (TextView) view;
        textView.setBackgroundColor(Color.GRAY);
        textView.setText(getClass().getSimpleName());
        return view;
    }
}
