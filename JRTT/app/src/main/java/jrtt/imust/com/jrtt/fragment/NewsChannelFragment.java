package jrtt.imust.com.jrtt.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.HashMap;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.bean.NewsChannelData;
import jrtt.imust.com.jrtt.fragment.sub.TabChannelFragment;
import jrtt.imust.com.jrtt.net.JrttApi;
import jrtt.imust.com.jrtt.net.ResponseData;
import jrtt.imust.com.jrtt.net.RetrofitUtil;
import jrtt.imust.com.jrtt.net.onDataLodListener;
import retrofit2.Call;

public class NewsChannelFragment extends BaseFragment implements onDataLodListener {

    private View view;
    private NewsChannelData data;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private HashMap<String,TabChannelFragment> pages = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        initViews();
        requestNet();
        return view;
    }

    @Override
    public void initViews() {
        view = View.inflate(getContext(), R.layout.fragment_newschannel,null);
        //获取指示器
        tabLayout = view.findViewById(R.id.tab_layout);
        //页面
        viewPager = view.findViewById(R.id.tab_viewpager);
    }

    @Override
    public void requestNet() {
        JrttApi api = RetrofitUtil.get();
        Call<ResponseData> call = api.getDataNews();
        //调用RetrofitUtil工具
        RetrofitUtil.send(getContext(),call,NewsChannelFragment.this);
    }

    @Override
    public void parsejson(String json) {
        System.out.print(json);
        Gson gson = new Gson();
        //注意：如果服务端返回一个json数据是[]
        //我们给.fromJson传参数  from()
        //{list:...}
        //一个class类对应一个json对象
        String  json_ = "{list:"+json+"}";
        data = gson.fromJson(json_, NewsChannelData.class);
        //System.out.print(data.list.size());
        //遍历一下这个页面多少个标题，一个标题对应一个新闻列表页面(三级),有多少标题就会对应
        //多少个页面
        for(NewsChannelData.ListBean item: data.list){
            //System.out.println(item.title+" "+item.url);
            TabChannelFragment tabChannelFragment = new TabChannelFragment();
            tabChannelFragment.setUrl(item.url);//每个页面都有访问地址
            pages.put(item.title,tabChannelFragment);
        }
    }

    @Override
    public void showData() {
        //1.先展示出来一个ViewPager
        NewAdapter adapter = new NewAdapter(getFragmentManager());
        //只要给viewPager设置一个适配器，他就可以产生多个子页面，可以滑动切换页面
        viewPager.setAdapter(adapter);
        //tabLayout可以指示页面叫什么名字，同时也可以指定页面属于第几个
        //用法非常简单，只要将有adapter的viewPager绑定给tabLayout
        tabLayout.setupWithViewPager(viewPager);

    }
    class NewAdapter extends FragmentPagerAdapter {
        public NewAdapter(FragmentManager fm) {
            super(fm);
        }

        //CharSequence字序列，把几个字符放在一起，不就是字符串
        //'a' 'b' 'c'
        @Nullable
        @Override
        //CharSequence
        public String getPageTitle(int position){//给ViewPager返回一个指定页面的标题
            NewsChannelData.ListBean bean = data.list.get(position);
            /*{
                "id": 10011,
                    "title": "财经",
                    "type": 1,
                    "url": "10011/list_1.json"
                }*/
            return bean.title;
        }

        @Override
        public Fragment getItem(int position) {
            /*ubFragment f = new SubFragment();
            NewsChannelData.ListBean bean = data.list.get(position);//

            f.setTitle(bean.title);*/

            //因为viewPager包含多个页面，每一个页面我们都保存在pages这个hashMap
            //viewpager的页面由它的adapter提供
            //getItem这个方法就是adpater，我们在这里机根据标题提供页面出来
            NewsChannelData.ListBean newBean = data.list.get(position);
            TabChannelFragment tabChannelFragment = pages.get(newBean.title);

            return tabChannelFragment;
        }

        @Override
        public int getCount() {
            return data.list.size();
        }
    }
}
