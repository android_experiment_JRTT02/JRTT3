package jrtt.imust.com.jrtt.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;

import java.util.List;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.bean.OneChannelData;

/**
 * Created by 10566 on 2020/6/19.
 */
//适配要提供给列表，几行，什么行试图，显示什么数据。1，集合2，视图3，显示逻辑
public class OneChannelQuickAdapter extends QuickAdapter<OneChannelData.NewsBean>{
    public OneChannelQuickAdapter(Context context, int layoutResId, List<OneChannelData.NewsBean> data) {
        super(context, layoutResId, data);
    }

    @Override
    protected void convert(BaseAdapterHelper helper, OneChannelData.NewsBean item) {
        //显示标题
        helper.setText(R.id.one_channel_new_title,item.title);
        //显示日期
        helper.setText(R.id.one_channel_new_date,item.pubdate);
        //加载图片
        ImageView imageView = helper.getView(R.id.one_channel_new_image);
        Glide.with(context).load(item.listimage).into(imageView);
    }
}
