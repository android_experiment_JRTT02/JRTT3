package jrtt.imust.com.jrtt.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import jrtt.imust.com.jrtt.R;
import jrtt.imust.com.jrtt.adapter.VideoQuickAdapter;
import jrtt.imust.com.jrtt.bean.VideoData;
import jrtt.imust.com.jrtt.net.JrttApi;
import jrtt.imust.com.jrtt.net.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
当前我们在这里开发一个视频列表的页面
    1.在fragment_video布局文件中，fangyige<ListView...
    2.可以访问video.json
    3.协商一个Retrofit的请求代码
    4.解析这个json数据
    5.不能直接把集合设置给ListView,而要创建一个ADapter
    6.再设置给列表ListView
 */
public class VideoFragment未实现接口 extends BaseFragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //1.在fragment_video布局文件中，fangyige<ListView...
        View v = View.inflate(getContext(), R.layout.fragment_video,null);
        //2.可以访问video.json
        final ListView listView = v.findViewById(R.id.listview);
        //3.协商一个Retrofit的请求代码
        //http://10.0.2.2:8080/jrtt/video.json
        String baseUrl = "http://10.0.2.2:8080/jrtt/";
        //3.1搞定retrofit对象
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
        //3.2请求需要的参数
        JrttApi api = retrofit.create(JrttApi.class);
        Call<ResponseData> call = api.getVideoData();
        //3.3放入请求不想写线程，来一个包含线程的请求队列
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                //拿到数据的情况
                ResponseData body = response.body();
                String json = body.data;
                //json
                System.out.print("成功"+json);
                //4.解析这个json数据
                Gson gson = new Gson();
                VideoData video = gson.fromJson(json, VideoData.class);
                System.out.print("有多少集："+video.data.vlist.size());//75
                //5.不能直接把集合设置给ListView,而要创建一个ADapter
                VideoQuickAdapter adapter =new VideoQuickAdapter(getContext(),R.layout.item_video,video.data.vlist);
                //6.再设置给列表ListView
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                //如果请求失败，提示
                Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
        return v;

    }
}
